<?php

namespace App\Repository;

use App\Entity\Fields;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Fields|null find($id, $lockMode = null, $lockVersion = null)
 * @method Fields|null findOneBy(array $criteria, array $orderBy = null)
 * @method Fields[]    findAll()
 * @method Fields[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FieldsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Fields::class);
    }

    // /**
    //  * @return Fields[] Returns an array of Fields objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('f.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Fields
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    public function findByDep($dep)
    {
        return $this->createQueryBuilder('d')
            ->where("d.identifiant LIKE :dep" )
            ->setParameter('dep', $dep.'%')
            ->getQuery()
            ->getResult();
    }

    public function findByCritere($critere)
    {
        return $this->createQueryBuilder('d')
            ->where("d.elem_patri LIKE :critere" )
            ->setParameter('critere', '%'.$critere.'%')
            ->getQuery()
            ->getResult();
    }

    public function findByCommune($commune){
        return $this->createQueryBuilder('d')
            ->where("d.commune LIKE :commune" )
            ->setParameter('commune', $commune.'%')
            ->getQuery()
            ->getResult();
    }

    public  function displayCommune(){
        return $this->createQueryBuilder('d')
            ->distinct()
            ->orderBy('d.commune')
            ->getQuery()
            ->getResult();

    }
}
