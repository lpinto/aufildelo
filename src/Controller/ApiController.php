<?php

namespace App\Controller;

use App\Entity\Fields;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class ApiController extends AbstractController
{

    /**
     * @Route("/api/departement/{departement}", name="departement", requirements={"departement"="\d+"})
     */

    public function findByDep(int $departement){
        $repository = $this->getDoctrine()->getRepository(Fields::class);
        $fields =$repository->findByDep($departement);
        return $this->json($fields);
    }

    /**
     * @Route("/api/critere/{critere}", name="critere")
     */
    public function findByCritere($critere)
    {
        $repository = $this->getDoctrine()->getRepository(Fields::class);
        $fields =$repository->findByCritere($critere);
        return $this->json($fields);
    }
    /**
     * @Route("/api/commune/{commune}", name="commune")
     */
    public function findByCommune($commune)
    {
        $repository = $this->getDoctrine()->getRepository(Fields::class);
        $fields =$repository->findByCommune($commune);
        return $this->json($fields);
    }
}
