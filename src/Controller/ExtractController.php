<?php

namespace App\Controller;

use App\Entity\Fields;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class ExtractController extends AbstractController
{
    /**
     * @Route("/extract", name="extract")
     */
    public function index()
    {
        $json = file_get_contents("../public/patrimoine-fluvial.json");
        $parsed_json = json_decode($json, true);
        $tabJson = [];

        for ($i = 0; $i < sizeof($parsed_json); $i++) {
            if (!empty($parsed_json[$i]['fields']['elem_princ'])) {
                $tabJson[$i] = ['commune' => $parsed_json[$i]['fields']['commune'],
                    'identifiant' => $parsed_json[$i]['fields']['identifian'],
                    'elem_patri' => $parsed_json[$i]['fields']['elem_patri'],
                    'elem_princ' => $parsed_json[$i]['fields']['elem_princ']];
            } else {
                $tabJson[$i] = ['commune' => $parsed_json[$i]['fields']['commune'],
                    'identifiant' => $parsed_json[$i]['fields']['identifian'],
                    'elem_patri' => $parsed_json[$i]['fields']['elem_patri'],
                    'elem_princ' => "NULL"];
            }
        }

        $file = "../public/datas.json";
        $current = json_encode($tabJson);
        //On écrit en Json dans le fichier
        file_put_contents($file, $current);

        $field = new Fields();
        $em = $this->getDoctrine()->getManager();
        foreach ($tabJson as $value) {
            $em->clear();
            $field->setCommune($value['commune']);
            $field->setIdentifiant($value['identifiant']);
            $field->setElemPatri($value['elem_patri']);
            $field->setElemPrinc($value['elem_princ']);

            $em->persist($field);
            $em->flush();
        }

        return $this->render('extract/index.html.twig', [
            'controller_name' => 'ExtractController',
        ]);
    }
}
